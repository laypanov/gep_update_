package com.example.gep_update;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.app.Activity;
import android.app.DownloadManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;


public class MainActivity extends Activity implements View.OnClickListener {
    ConstraintLayout ConstraintLayout;
    android.widget.LinearLayout LinearLayout;

    private TextView textViewNameApp;
    private TextView textViewVNameApp;
    private TextView textViewCv;
    private TextView textViewDV;
    private TextView textView_IDate;
    private TextView textView_IVersion;
    private TextView textViewStatus;
    private Spinner spinnerVersions;
    private View ViewV;

    private DateFormat f = new SimpleDateFormat("yyyy-MM-dd");
    private long downloadID;


    public void getNewVersion(View view)
    {
        ImageButton btn3 = (ImageButton) this.findViewById(view.getId());
        ConstraintLayout layout = (ConstraintLayout) btn3.getParent();

        for(int i=0;i<layout.getChildCount();i++)
        {
            View v =  (View)layout.getChildAt(i);
            if (v instanceof Spinner) {
                Spinner vSpinner = (Spinner) layout.getChildAt(i);
                String text = vSpinner.getSelectedItem().toString();
                String[] t = text.split("\\[");
                t = t[1].split("]");
                DownloadFiles(t[0]);
            }
        }
    }

    public void setNewLayout(int i, String[] Arr_common, ArrayList<String> Arr_versions) throws ParseException {

        String textViewNameAppId = "textViewNameApp_" + i;
        String textViewVNameAppId = "textViewVNameApp_" + i;
        String textViewCvId = "textViewCv_" + i;
        String textViewDVId = "textViewDV_" + i;
        String textView_IDateId = "textView_IDate_" + i;
        String textView_IVersionId = "textView_IVersion_" + i;
        String textViewStatusId = "textViewStatus_" + i;
        String spinnerVersionsId = "spinnerVersions_" + i;
        String ConstraintLayoutId  = "cl" + i;
        String ViewId  = "cl" + i;

        int textViewNameAppIdN = getResources().getIdentifier(textViewNameAppId, "id", this.getPackageName());
        int textViewVNameAppIdN = getResources().getIdentifier(textViewVNameAppId, "id", this.getPackageName());
        int textViewCvIdN = getResources().getIdentifier(textViewCvId, "id", this.getPackageName());
        int textViewDVIdN = getResources().getIdentifier(textViewDVId, "id", this.getPackageName());
        int textView_IDateIdN = getResources().getIdentifier(textView_IDateId, "id", this.getPackageName());
        int textView_IVersionIdN = getResources().getIdentifier(textView_IVersionId, "id", this.getPackageName());
        int textViewStatusIdN = getResources().getIdentifier(textViewStatusId, "id", this.getPackageName());
        int spinnerVersionsIdN = getResources().getIdentifier(spinnerVersionsId, "id", this.getPackageName());
        int ConstraintLayoutIdN = getResources().getIdentifier(ConstraintLayoutId, "id", this.getPackageName());
        int ViewIdN = getResources().getIdentifier(ViewId, "id", this.getPackageName());

        textViewNameApp = (TextView) this.findViewById(textViewNameAppIdN);
        textViewVNameApp = (TextView) this.findViewById(textViewVNameAppIdN);
        textViewCv = (TextView) this.findViewById(textViewCvIdN);
        textViewDV = (TextView) this.findViewById(textViewDVIdN);
        textView_IDate = (TextView) this.findViewById(textView_IDateIdN);
        textView_IVersion = (TextView) this.findViewById(textView_IVersionIdN);
        textViewStatus = (TextView) this.findViewById(textViewStatusIdN);
        spinnerVersions = (Spinner) this.findViewById(spinnerVersionsIdN);
        ConstraintLayout = (ConstraintLayout) this.findViewById(ConstraintLayoutIdN);
        ViewV = (ConstraintLayout) this.findViewById(ViewIdN);


        ConstraintLayout.setVisibility(View.VISIBLE);
        ViewV.setVisibility(View.VISIBLE);

        String[] arr;
        arr = getTabletVersion(Arr_common[0]);
        // название
        textViewNameApp.setText(String.valueOf(Arr_common[1]));
        // навазние пакета
        textViewVNameApp.setText(String.valueOf(Arr_common[0]));
        // текущая версия
        textViewCv.setText(String.valueOf(arr[0]));
        // дата установки ТВ
        textViewDV.setText(String.valueOf(arr[1]));
        // новая версия
        textView_IDate.setText(String.valueOf(Arr_common[2]));
        // дата добавления на сервер
        textView_IVersion.setText(String.valueOf(Arr_common[3]));

        //System.out.println(Arr_versions.spliterator("22"));
        ArrayList<String> arr_data1 = new ArrayList<String>();
        for (int i3 = 0; i3 < Arr_versions.size(); i3++) {
            String arr3 = Arr_versions.get(i3);
            String[] arr3_data = arr3.split(",");
            arr_data1.add(" версия: " + arr3_data[0] + " от" + arr3_data[1] + "\n [" + arr3_data[2] + "]");
        }

        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this,R.layout.spinner_item, arr_data1);

        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        spinnerVersions.setAdapter(spinnerArrayAdapter);

        try {
            Date date = f.parse(String.valueOf(arr[1]));
            Date date2 = f.parse(String.valueOf(Arr_common[3]));

            long time = date.getTime();
            java.sql.Timestamp ts = new java.sql.Timestamp(time);
            long time2 = date2.getTime();
            java.sql.Timestamp tsC = new java.sql.Timestamp(time2);

            if (ts.compareTo(tsC) >= 0) {
                textViewStatus.setText("[актуальная версия]");
            } else if (ts.compareTo(tsC) < 0) {
                textViewStatus.setText("[обновление]");
                textViewStatus.setTextColor(Color.parseColor("#0000FF"));
            } else {
                textViewStatus.setText("[данные не плучены]");
                textViewStatus.setTextColor(Color.parseColor("#FF0000"));
            }
        } catch (ParseException e) {
            textViewStatus.setText("[не установлено]");
            textViewStatus.setTextColor(Color.parseColor("#FF0000"));
        }

    }

    private String[] getTabletVersion(String packageName) {
        PackageInfo packageInfo = null;
        String[] ArrCv = new String[0];
        try {
            packageInfo = getPackageManager().getPackageInfo(packageName, 0);

            long packagelastUpdateTime = getPackageManager().getPackageInfo(packageName, 0).lastUpdateTime;

            long timestamp2 = Long.valueOf(packagelastUpdateTime);
            Date d2 = new Date(timestamp2);
            String plut = f.format(d2);
            String pv = packageInfo.versionName;

            ArrCv  = new String[] {pv, plut, String.valueOf(timestamp2)};

        } catch (PackageManager.NameNotFoundException e) {
            return ArrCv  = new String[] {"---", "---", "---"};
        }
        return ArrCv;
    }

    public void setLayoutParamsText(String currentText, String old, ConstraintLayout ConstraintLayoutV) {
        ConstraintSet set = new ConstraintSet();

        TextView textViewOldVar;

        int idOldVar = getResources().getIdentifier(old, "id", this.getPackageName());
        textViewOldVar = (TextView) this.findViewById(idOldVar);
        TextView textViewVar = new TextView(this);

        textViewVar.setText(String.valueOf(currentText));

        textViewVar.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        ViewGroup.MarginLayoutParams marginParams = new ViewGroup.MarginLayoutParams(textViewVar.getLayoutParams());
        int left_margin = 102;
        int top_margin = 102;
        int right_margin = 10;
        int bottom_margin = 10;
        marginParams.setMargins(
                marginParams.leftMargin,
                marginParams.topMargin,
                150,
                marginParams.bottomMargin);

        textViewVar.setBackgroundColor(0xff66ff66);
        ConstraintLayout parentLayout = (ConstraintLayout)findViewById(R.id.cl3);
        textViewVar.setId(View.generateViewId());

        ConstraintLayoutV.addView(textViewVar, 0);

        ConstraintSet set2 = new ConstraintSet();

        set2.clone(parentLayout);
        set2.connect(textViewVar.getId(), ConstraintSet.TOP, parentLayout.getId(), ConstraintSet.TOP, 60);
        set2.applyTo(parentLayout);

        TextView textViewOldVStatus;
        int idOldVStatus = getResources().getIdentifier("textViewStatus", "id", this.getPackageName());
        textViewOldVStatus = (TextView) this.findViewById(idOldVStatus);
        TextView textViewStatus2 = new TextView(this);
        textViewStatus2.setText(String.valueOf("АКТУАЛЬНАЯ"));
    }
    private ConstraintSet mConstraintSetNormal = new ConstraintSet();
    private ConstraintSet mConstraintSetClone = new ConstraintSet();

    private BroadcastReceiver onDownloadComplete = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Toast.makeText(MainActivity.this, "Загрузка завершена", Toast.LENGTH_SHORT).show();
            long id = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
            System.out.println(downloadID);
            System.out.println(id);

            if (downloadID == id) {
                File downLoadApk = new File(Environment.getExternalStorageDirectory(),"/download/tmp.apk");
                if (downLoadApk.exists()) {
                    Intent install = new Intent(Intent.ACTION_VIEW);
                    install.setDataAndType(Uri.fromFile(new File(Environment.getExternalStorageDirectory() +  "/download/tmp.apk")), "application/vnd.android.package-archive");
                    install.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(install);
                }
            }

        }
    };
    @Override
    public void onDestroy() {
        super.onDestroy();
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Context context = this;
        ConstraintLayout = findViewById(R.id.cl3);
        LinearLayout = findViewById(R.id.ll1);
        ConstraintSet set = new ConstraintSet();

        ConstraintLayout ConstraintLayout2 = new ConstraintLayout(this);
        LinearLayout.addView(ConstraintLayout2);

        requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 101);
        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 102);


        onRefreshPage();
    }

    public void onRefreshPage(){

        ArrayList<String> Arr_versions = new ArrayList<String>();
        String[] Arr_common  = new String[] {};
        JSONArray jArray_tt_info = getVerisionsPackeges();
        String name_old = null;

        int i2 = 0;
        for (int i = 0; i < jArray_tt_info.length(); i++) {
            try {
                JSONObject oneObject = jArray_tt_info.getJSONObject(i);

                String program = oneObject.getString("program__name");
                String name = oneObject.getString("name_program");
                Boolean actual = oneObject.getBoolean("actual");
                String build = oneObject.getString("build");
                String date = oneObject.getString("date");
                String link = oneObject.getString("link");

                if(name_old == null){
                    name_old = name;
                }
                if(!name_old.equals(name)){
                    try {
                        setNewLayout(i2, Arr_common, Arr_versions);
                        Arr_versions.clear();
                        if (actual) {
                            i2 = i2 + 1;
                            Arr_common = new String[]{name, program, build, date};
                        }
                        Arr_versions.add(build + ", " + date + "," + link);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }else{
                    if (actual) {
                        i2 = i2 + 1;
                        Arr_common = new String[]{name, program, build, date};
                    }
                    Arr_versions.add(build + ", " + date + "," + link);
                }
                if (jArray_tt_info.length() -1  == i ) {
                    try {
                        setNewLayout(i2, Arr_common, Arr_versions);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                name_old = name;

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        PackageInfo packageInfo = null;
        try {
            packageInfo = getPackageManager().getPackageInfo("com.example.myapplication", 0);
            long packageinstalltime = getPackageManager().getPackageInfo("com.example.myapplication", 0).firstInstallTime;
            long packagelastUpdateTime = getPackageManager().getPackageInfo("com.example.myapplication", 0).lastUpdateTime;;


            Long tsLong = System.currentTimeMillis();
            String ts = tsLong.toString();

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        new Thread(new Runnable() {
            public void run() {}
        }).start();
    }

    public JSONArray getVerisionsPackeges() {
        try {
            String address_server = "";
            JSONArray jsonParam_common = new JSONArray();
            JSONObject jsonObject_common_buffer = new JSONObject();
            JSONObject rootObject = new JSONObject();
            int i;

            jsonObject_common_buffer.put("process", "JSON");
            jsonObject_common_buffer.put("post_url", "http://" + address_server + "/?get_info=1");

            jsonParam_common.put(jsonObject_common_buffer);

            rootObject.put("common", jsonParam_common);

            String json_data = new PostImageToServerAsync().execute(rootObject).get();
            if (json_data.equals("False")) {
                Toast.makeText(this, "Отсутствует соединение с сервером", Toast.LENGTH_LONG).show();
                return jsonParam_common;
            }
            JSONObject jObject = new JSONObject(json_data);
            JSONArray jArray_tt_info = jObject.getJSONArray("versions");

            return jArray_tt_info;

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }


    public void DownloadFiles(String url) {
        try {
            File downLoadApk = new File(Environment.getExternalStorageDirectory(),"/download/tmp.apk");

            downLoadApk.delete();
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            final Uri uri;
            String destination;

            url = "" + url;
            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
                destination = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/";
                String fileName = "tmp.apk";
                destination += fileName;
                uri = Uri.parse("file://" + destination);


                File file2 = new File(uri.getPath());
                file2.delete();
                if(file2.exists()){
                    file2.getCanonicalFile().delete();
                    if(file2.exists()){
                        getApplicationContext().deleteFile(file2.getName());
                    }
                }

                request.setTitle("tmp.apk");
                request.setDestinationUri(uri);
            }else {
            }
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                request.setDestinationInExternalFilesDir(this, Environment.DIRECTORY_DOWNLOADS, "tmp.apk");
            }
            final DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);

            final long downloadId = manager.enqueue(request);

            BroadcastReceiver onComplete = new BroadcastReceiver() {
                public void onReceive(Context ctxt, Intent intent) {
                    String action = intent.getAction();
                    if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
                        Bundle extras = intent.getExtras();
                        DownloadManager.Query q = new DownloadManager.Query();
                        q.setFilterById(extras.getLong(DownloadManager.EXTRA_DOWNLOAD_ID));
                        Cursor c = manager.query(q);
                        if (c.moveToFirst()) {
                            int status = c.getInt(c.getColumnIndex(DownloadManager.COLUMN_STATUS));
                            if (status == DownloadManager.STATUS_SUCCESSFUL) {
                                String fullPath = null; File source = null;
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    fullPath = c.getString(c.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI));
                                    source = new File(Uri.parse(fullPath).getPath());
                                    System.out.println(source);

                                    if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
                                        DownloadManager.Query query = new DownloadManager.Query();
                                        query.setFilterById(downloadId);
                                        Cursor cursor = manager.query(query);

                                        System.out.println(DownloadManager.STATUS_SUCCESSFUL);
                                        Intent install = new Intent(Intent.ACTION_VIEW);
                                        install.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);


                                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
                                            install.setDataAndType(Uri.fromFile(new File(Uri.parse(fullPath).getPath())), "application/vnd.android.package-archive");
                                        }else{
                                            Uri apkUri = FileProvider.getUriForFile(getApplicationContext(),
                                                    "com.example.gep_update.fileprovider", new File(Uri.parse(fullPath).getPath()));

                                            install = new Intent(Intent.ACTION_INSTALL_PACKAGE);
                                            install.setData(apkUri);
                                            install.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                        }
                                        startActivity(install);
                                        unregisterReceiver(this);
                                    }
                                } else {
                                    fullPath = c.getString(c.getColumnIndex(DownloadManager.COLUMN_LOCAL_FILENAME));
                                    source = new File(fullPath);
                                    System.out.println(source);
                                }
                            }
                        }
                        c.close();
                    }
                }
            };
            registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


        public void onClick_sync_action_btn(View view) throws Exception {
        try {
            String address_server = "";
            JSONArray jsonParam_common = new JSONArray();
            JSONObject jsonObject_common_buffer = new JSONObject();
            JSONObject rootObject = new JSONObject();
            int i;

            jsonObject_common_buffer.put("process", "JSON");
            jsonObject_common_buffer.put("post_url", "http://" + address_server + "/?get_info=1");

            jsonParam_common.put(jsonObject_common_buffer);

            rootObject.put("common", jsonParam_common);

            String json_data = new PostImageToServerAsync().execute(rootObject).get();

            JSONObject jObject = new JSONObject(json_data);
            JSONArray jArray_tt_info = jObject.getJSONArray("versions");

            for (i = 0; i < jArray_tt_info.length(); i++) {
                JSONObject oneObject = jArray_tt_info.getJSONObject(i);

                String program = oneObject.getString("program__name");
                String actual = oneObject.getString("actual");
                String build = oneObject.getString("build");
                String date = oneObject.getString("date");
                String link = oneObject.getString("link");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {

    }
}
